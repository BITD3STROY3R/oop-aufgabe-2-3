/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pumpkin;

import java.util.Random;

import simulation.Range;

public class Pumpkin implements Comparable<Pumpkin> {
	private String breed;
	// Required conditions for Growth
	// dimension 1
	private int weight = 50;
	// days, minimum time remaining to ripe
	private int timeToRipe;
	// days, minimum time to ripe
	private int timeRipe;
	// if ripe, growth stops
	private boolean ripe;
	// percent
	private int resistanceToPutrefaction;
	// maximum growth, percent
	private int growth;
	// hours
	private Range<Double> light;
	// percent
	private Range<Integer> humidity;
	// percent
	private Range<Integer> fertility;
	// degree celsius
	private Range<Integer> temperature;
	
	// Resource consumption
	// percent
	private int humidityConsumption;
	// percent
	private int fertilityConsumption;
	
	@Override
	public int compareTo(Pumpkin other) {
		int result = 0;
		if (this.weight > other.weight) {
			result = 1;
		} else if (this.weight < other.weight) {
			result = -1;
		}
		return result;
	}
	
	public void grow(int effectiveGrowth) {
			weight += weight * effectiveGrowth / (double) 100;
			timeToRipe -= timeToRipe == 0 ? 0 : 1;
	}
	
	public boolean isRipe() {
		return ripe;
	}

	
	public void decreaseWeightPercent(int percent) {
		this.weight -= this.weight * (double) percent / 100;
		weight = weight < 0 ? 0 : weight;
	}
	
	public void decreseWeightAbsolute(int gramm) {
		weight -= gramm;
		weight = weight < 0 ? 0 : weight;
	}
	
	public String getBreed() {
		return breed;
	}
	
	public Pumpkin crossOver(Pumpkin other) {
		
		double low, high;
		Random r = new Random();
		
		// create new breed name
		String newBreed = "(" + this.breed + " x " + other.breed + ")";
		
		// interpolate fertility
		 low = interpolate(this.fertility.getLow(), other.fertility.getLow(),
				 r.nextDouble());
		 high = interpolate (this.fertility.getHigh(), other.fertility.getHigh(),
				r.nextDouble());
		Range<Integer> newFertility = new Range<Integer>((int)low, (int)high);
		
		// interpolate humidity
		low = interpolate(this.humidity.getLow(), other.humidity.getLow(),
				r.nextDouble());
		high = interpolate(this.humidity.getHigh(), other.humidity.getLow(),
				r.nextDouble());
		Range<Integer> newHumidity = new Range<Integer>((int)low, (int)high);
		
		// interpolate temperature
		low = interpolate(this.temperature.getLow(), other.temperature.getLow(),
						r.nextDouble());
		high = interpolate(this.temperature.getHigh(), other.temperature.getHigh(),
				r.nextDouble());
		Range<Integer> newTemperature = new Range<Integer>((int)low, (int)high);
		
		// interpolate light
		low = interpolate(this.light.getLow(), other.light.getLow(),
				r.nextDouble());
		high = interpolate(this.light.getHigh(), other.light.getHigh(),
				r.nextDouble());
		Range<Double> newLight = new Range<Double>(low, high);
		
		// interpolate growth speed
		int newGrowth = 
				(int) interpolate(this.growth, other.growth,
						r.nextDouble()
						);
		
		// interpolate putrefaction resistance
		int newResistanceToPutrefaction =
				(int) interpolate(
						this.resistanceToPutrefaction,
						other.resistanceToPutrefaction,
						r.nextDouble()
						);
		
		// interpolate ripe time
		int newRipeTime = 
				(int) interpolate(this.timeRipe, other.timeRipe, r.nextDouble());
		
		// interpolate ressource consumption
		int newHumidityConsumption =
				(int) interpolate(
						this.humidityConsumption, 
						other.humidityConsumption, 
						r.nextDouble()
						);
		
		int newFertilityConsumption =
				(int) (r.nextDouble()
				* (this.fertilityConsumption - other.fertilityConsumption)
				+ this.fertilityConsumption);
		
		
		return new PumpkinCrossBreed (
				newBreed,
				newHumidity,
				newFertility,
				newTemperature,
				newLight,
				newRipeTime,
				newResistanceToPutrefaction,
				newGrowth,
				newFertilityConsumption,
				newHumidityConsumption);
	}
	 
	@Override
	public String toString() {
		return	"Pumpkin Breed = " + breed
				+ "; weight = " + weight
				+ "; timeToRipe = " + timeToRipe
				+ "; isRipe = " + ripe;
	}
	
	public Pumpkin(
			String breed,
			Range<Integer> humidity,
			Range<Integer> fertility,
			Range<Integer> temperature,
			Range<Double> light,
			int timeRipe,
			int resistanceToPutrefaction,
			int growth,
			int fertilityConsumption,
			int humidityConsumption ) {
		this.breed = breed;
		this.humidity = humidity;
		this.fertility = fertility;
		this.temperature = temperature;
		this.light = light;
		this.timeRipe = timeRipe;
		this.timeToRipe = timeRipe;
		this.resistanceToPutrefaction = resistanceToPutrefaction;
		this.growth = growth;
		this.fertilityConsumption = fertilityConsumption;
		this.humidityConsumption = humidityConsumption;
	}
	
	private <T extends Number> double interpolate(T t1, T t2, double x) {
		return new Double(x * (t2.doubleValue() - t1.doubleValue())
				+ t1.doubleValue());
	}
	
	public int getHumidityConsumption() {
		return humidityConsumption;
	}
	
	public int getFertilityConsumption() {
		return fertilityConsumption;
	}
	
	public int getWeight() {
		return weight;
	}

	public int getTimeToRipe() {
		return timeToRipe;
	}

	public int getResistanceToPutrefaction() {
		return resistanceToPutrefaction;
	}

	public int getGrowth() {
		return growth;
	}

	public Range<Double> getLight() {
		return light;
	}

	public Range<Integer> getHumidity() {
		return humidity;
	}

	public Range<Integer> getFertility() {
		return fertility;
	}

	public Range<Integer> getTemperature() {
		return temperature;
	}

	public void setRipe(boolean ripe) {
		this.ripe = ripe;
	}

	public void setResistanceToPutrefaction(int resistanceToPutrefaction) {
		this.resistanceToPutrefaction = resistanceToPutrefaction;
	}

	
	public static void main(String[] args) {
		Pumpkin p1 = new Hokaido();
		Pumpkin p2 = new Butternut();
		
		Pumpkin p3 = p1.crossOver(p2);
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
	}
}