/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pumpkin;

import simulation.Range;

public final class Butternut extends Pumpkin {

	public Butternut() {
		super(
				"Butternut",	
				new Range<Integer>(70,95),		// humidity
				new Range<Integer>(30, 50),		// Fertility
				new Range<Integer>(18, 30),		// temperature
				new Range<Double>(8.0, 12.0),	// light
				120,							// days of growth
				10,								// resistance to putrefaction
				8,								// growth 
				20, 							// fertility consumption
				25	 							// humidity consumption
		);
	}
}
