/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pumpkin;

import simulation.Range;

public final class TestPumpkin extends Pumpkin {
	
	public TestPumpkin() {
		super(
				"Test Pumpkin",	
				new Range<Integer>(30,60),		// humidity
				new Range<Integer>(50, 80),		// Fertility
				new Range<Integer>(10, 25),		// temperature
				new Range<Double>(2.0, 7.0),	// light
				2,							// days of growth
				55,								// resistance to putrefaction
				3,								// growth 
				15, 							// fertility consumption
				20	 							// humidity consumption
		);
	}
}
