/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package weather;

import java.util.List;
import java.util.GregorianCalendar;


//test
public class Test {
	
	public static void main(String[] args) {
		GregorianCalendar start = new GregorianCalendar(2000, 0, 1);
		GregorianCalendar end = new GregorianCalendar(2000, 11, 31);
		
		WeatherGenerator gen = new AustrianWeatherGenerator(start, end);
		
		
		List<WeatherCondition> weather = gen.getWeather();
		
		int i = 1;
		
		for (WeatherCondition wc : weather) {
			System.out.println(i++ + " " + wc);
		}
		
		System.out.println("\n");
		
		List<WeatherCondition> wf = new WeatherForecast(weather).getWeatherForecast(7);
		
		i = 1;
		
		for (WeatherCondition wc : wf) {
			System.out.println(i++ + " " + wc);
		}
	}

}
