/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package weather;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WeatherForecast {
	
	private List<WeatherCondition> data;
	
	// Zusicherung: data != null
	public WeatherForecast(List<WeatherCondition> data){
		this.data = data;
	}
	
	private double getUniformRandom(double mean, double deviation){
		Random r = new Random();
		return r.nextGaussian() * deviation + mean;
	}
	
	private double round(double num, int places){
		double n = Math.pow(10, places);
		return Math.round((num * n))/n;
	}
	
	// Vorbedingung days >= 0
	// Nachbedingung: liefert liste mit days Eintraegen, die den Forecast enthalten
	// GUT: Dynamisches Binden mit List, erleichtert das getten der Werte
	public List<WeatherCondition> getWeatherForecast(int days){
		assert(days < 0);
		List<WeatherCondition> forecast = new ArrayList<WeatherCondition>();
		
		for (int i = 0; i < days; i++) {
			double divisor = 0;
			double rainDivisor = 0;
			
			double high_avg = 0;
			double low_avg = 0;
			double wind_avg = 0;
			double sunHours_avg = 0;
			double rain_avg = 0;
			double rainyDays = 0;
			
			
			for (int j = data.size()-1; j >= 0; j--) {
				double factor = Math.pow(2, j);
				divisor += factor;
				
				WeatherCondition wc = data.get(j);
				
				low_avg += factor * wc.getLow();
				high_avg += factor * wc.getHigh();
				wind_avg += factor * wc.getWind();
				sunHours_avg = factor * wc.getWind();
				
				if (wc.getRain() > 0) {
					rainyDays++;
					rainDivisor += factor;
					rain_avg += factor * wc.getRain();
				}
			}
			
			if ((rainyDays / data.size()) < getUniformRandom(0.4, 0.1)) {
				if (rain_avg == 0) {
					rain_avg = getUniformRandom(1.5, 0.2);
				}else{
					rain_avg = getUniformRandom(rain_avg / rainDivisor, 0.2);
				}
			}
			else{
				rain_avg = 0;
			}
			
			rain_avg = round(rain_avg, 2);
			
			low_avg = getUniformRandom(low_avg / divisor, 1);
			low_avg = round(low_avg, 1);
			
			high_avg = getUniformRandom(high_avg / divisor, 1);
			high_avg = round(high_avg, 1);
			
			wind_avg = getUniformRandom(wind_avg / divisor, 1);
			wind_avg = round(wind_avg, 1);
			
			do {
				sunHours_avg = getUniformRandom(sunHours_avg / divisor, 1);
			} while (sunHours_avg < 0);
			sunHours_avg = round(sunHours_avg, 0);
			
			WeatherCondition wc = new WeatherCondition(high_avg, low_avg, rain_avg, sunHours_avg, wind_avg);
			
			data.add(wc);
			forecast.add(wc);
		}
		
		return forecast;
	}

}
