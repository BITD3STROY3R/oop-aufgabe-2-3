/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package weather;

import java.util.Calendar;
import java.util.List;

public abstract class WeatherGenerator {
	protected Calendar start, end;
	
	// start != null, end != null
	public WeatherGenerator(Calendar start, Calendar end){
		this.start = start;
		this.end = end;
	}
	
	// GUT: Dynamisches Binden mit List, erleichtert das getten der Werte
	abstract public List<WeatherCondition> getWeather();
	
}
