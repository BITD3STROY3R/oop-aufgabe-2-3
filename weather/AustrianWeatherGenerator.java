/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package weather;

import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

// Vorbedingungen unverändert
public class AustrianWeatherGenerator extends WeatherGenerator {
	
	private ArrayList<WeatherCondition> weather;

	private double sinc(double x, double max, double min){
		max = max - min;
		double expr = (2*Math.PI*x)-Math.PI;
		return max * (Math.sin(expr)/(expr)) + min;
	}
	
	private double getUniformRandom(double mean, double deviation){
		Random r = new Random();
		return r.nextGaussian() * deviation + mean;
	}
	
	private double round(double num, int places){
		double n = Math.pow(10, places);
		return Math.round((num * n))/n;
	}
	
	// Invariante: Alle Wettervariablen > 0
	private WeatherCondition GenerateWeatherForADay(Calendar date){
		WeatherCondition today = null;
		Random r = new Random();
		
		double meanhigh, meanlow, sunHours, rain, high, low;

		double day = date.get(Calendar.DAY_OF_YEAR);
		double normDay = (day-15)/365.0;
		
		double wind = getUniformRandom(9, 3);
		
		double rainprob = sinc(normDay, 0.45, 0.3);
		
		boolean rainy = false;
		
		if (r.nextDouble() < rainprob) {
			rainy = true;
			meanhigh = sinc(normDay, 26, -5); 
			meanlow = sinc(normDay, 12, -10); 

			double meanrain = sinc(normDay, 2, 1);
			rain = getUniformRandom(meanrain, 0.2*meanrain);
			
			sunHours = sinc(normDay, 6.5, 1);
		}
		
		else{
			meanhigh = sinc(normDay, 30, -1); 
			meanlow = sinc(normDay, 14, -5); 
			
			rain = 0;
			sunHours = sinc(normDay, 10, 1);
		}
		
		high = getUniformRandom(meanhigh, 1);
		low = getUniformRandom(meanlow, 1);
		
		do {
			sunHours = getUniformRandom(sunHours, 1);
		} while (sunHours < 0);

		high = round(high , 1);
		low = round(low , 1);
		rain = round(rain, 2);
		sunHours = round(sunHours, 0);
		wind = round(wind, 1);
		
		if (!(today instanceof HeavyRainCondition)) {
			if (r.nextDouble() < 0.06) {
				if (rainy)
					today = new HeavyRainCondition(high, low, sunHours, wind);
				else if (day > 180 && day < 250) 
					today = new HeatCondition(wind);
				else
					today = new StormCondition(high, low, rain, sunHours);
			}
			else{
				today = new WeatherCondition(high, low, rain, sunHours, wind);
			}
		}

		return today;
	}
	
	private void GenerateWeather(){
		while(!start.after(end)){
			weather.add(GenerateWeatherForADay(start));
		    start.add(Calendar.DATE, 1);
		}
	}
	
	// Vorbedingung start und end = gültiges Datum
	public AustrianWeatherGenerator(Calendar start, Calendar end){
		super(start, end);
		weather = new ArrayList<WeatherCondition>();
		GenerateWeather();
	}
	
	
	// Zusicherung: Liste enthält genauso so viele weatherconditions wie Tage zwischen start und end
	@Override
	public List<WeatherCondition> getWeather(){
		return weather;
	}
	
}
