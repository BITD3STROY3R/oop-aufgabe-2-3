/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package weather;
public class WeatherCondition {
	/*
	
	Einheiten
	
	high, low - Grad Celsius
	rain - Regen in l/m^2/24h
	wind - Durchschnittliche Windgeschwindigkeit
	
	*/
	private double high, low, rain, sunHours, wind;
	
	public WeatherCondition(double high, double low, double rain, double sunHours, double wind) {
		this.high = high;
		this.low = low;
		this.rain = rain;
		this.sunHours = sunHours;
		this.wind = wind;
	}
	
	public double getHigh() {
		return high;
	}

	public double getLow() {
		return low;
	}

	public double getRain() {
		return rain;
	}

	public double getSunHours() {
		return sunHours;
	}

	public double getWind() {
		return wind;
	}

	public String toString(){
		return "High/Low: " + high +"/"+ low + " // " + "Rain: " + rain + " // " + "Wind: " + wind + " // " + "Sun Hours: " + sunHours;
	}
}
