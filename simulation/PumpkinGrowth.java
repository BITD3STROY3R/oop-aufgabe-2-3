/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import pumpkin.Pumpkin;

public class PumpkinGrowth implements EnvironmentAlteration, PumpkinAlteration {

	// invariante: pumpkin != null
	private Pumpkin pumpkin;
	
	// invariante: environemnt != null
	private Environment environment;
	
	// vorbedingung pumpkin != null, environment != null
	public PumpkinGrowth(Pumpkin pumpkin, Environment environment) {
		this.pumpkin = pumpkin;
		this.environment = environment;
	}
	
	@Override
	public boolean hasWornOff() {
		return pumpkin.isRipe();
	}
	
	/* GUT durch die Doppelvererbung wurde Abschwächung der
	 * Nachbedingungen wie in der Klasse PestInfluence umgangen
	 */
	
	/* vorbedingung: p.equals(pumpkin)
	 * nachbedingung: lässt p den Bedingungen entsprechend einmal
	 * wachsen
	 */
	@Override
	public void apply(Pumpkin p) {
		
		// ripe pumpkin don't grow anymore
		if (!p.isRipe()) {
			
			// contribution of fertility
			int fertility = environment.getSoil().getFertility();
			double cf = 1 - pumpkin.getFertility().adMid(fertility);
			
			// contribution of light
			double light = environment.getLight();
			double cl = 1 - pumpkin.getLight().adMid(light);
			
			// contribution of temperature
			Range<Integer> temperature = environment.getTemperature();
			double ct = pumpkin.getTemperature().overlap(temperature);
			
			// contribution of humidity
			int humidity = environment.getHumidity();
			double ch = 1 - pumpkin.getHumidity().adMid(humidity);
			
			// calculate effective growth
			int maxGrowth = p.getGrowth();
			int effGrowth = (int)Math.round(maxGrowth * (cf + cl + ct + ch));
			
			// apply growth
			pumpkin.grow(effGrowth);
		}
	}

	/* vorbedingung: e.equals(environment)
	 * nachbedingung: übt die einflüsse des Kürbiswachstums
	 * auf e aus.
	 */
	@Override
	public void apply(Environment e) {
		
		// set the new humidity
		e.decreaseHumidity(pumpkin.getHumidityConsumption());
		
		// set the new fertility
		e.getSoil().decreaseFertility(pumpkin.getFertilityConsumption());
	}

}
