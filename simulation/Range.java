/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

public class Range<T extends Number & Comparable<T>> {
	
	// low != null und high != null und low < high
	private T low;
	private T high;
	
	// low != null und high != null und low < high
	public Range(T low, T high) {
		assert(low.compareTo(high) < 0);
		
		this.low	= low;
		this.high	= high;
	}
	
	// vorbedingung: x != null
	/**
	 * Returns true if x is between low, and high (inclusive).
	 */
	public boolean isIn(T x) {
		return low.compareTo(x) < 1 && high.compareTo(x) >= 0;
	}
	
	// vorbedingung: x != null
	/**
	 * Returns the relative distance to low (in [0, 1])
	 */
	public double rel(T x) {
		return Math.abs(low.doubleValue() - x.doubleValue())
				/ Math.abs(low.doubleValue() - high.doubleValue());
	}
	
	// vorbedingung: x != null
	/**
	 * For x in [low, high] this method returns the normalized absolute 
	 * distance to the range's middle. If x not in [low, high] the it returns 1.0
	 */
	public double adMid(T x) {
		double mid = Math.abs(low.doubleValue() + high.doubleValue()) / 2;
		double result = Math.abs(x.doubleValue() - mid)
				/ (Math.abs(low.doubleValue() - high.doubleValue())/2);
		result = result > 1 ? 1 : result;
		return result;
	}
	
	// nachbedingung: 0 < result
	/**
	 * Returns the width of the Range
	 */
	public double width() {
		return Math.abs(low.doubleValue() - high.doubleValue());
	}
	
	/* vorbedingung: other != null 
	 * und (this und other schneiden sich in mehr als einen Punkt)
	 */
	/**
	 * Returns the normalized width of the overlapping area (normalized to this.width())
	 * [!!] this != [x,x]
	 */
	public double overlap(Range<T> other) {
		return this.intersect(other).width() / this.width();
	}
	
	// other != null
	/**
	 * Returns the intersection of this and other
	 */
	public Range<T> intersect(Range<T> other) {
		Range<T> r1 = this.low.compareTo(other.low) > 0 ? this : other;
		Range<T> r2 = this.high.compareTo(other.high) < 0 ? this : other;
		Range<T> result;
		
		if (r1 != r2) {
			result = new Range<T>(r1.low, r2.high);
		} else {
			if (r1.low.compareTo(r1.high) > 0) {
				result = new Range<T>(r1.low, r1.low);
			} else {
				result = new Range<T>(r1.low, r1.high);
			}
		}
		return result;
	}
	
	public T getLow() {
		return low;
	}
	
	public T getHigh() {
		return high;
	}
	
	@Override
	public String toString() {
		return "[" + low + ", " + high + "]";
	}
	
	public static void main(String[] args) {
		Range<Double> r1 = new Range<Double>(0.0, 4.0);
		
		System.out.println(r1.adMid(2.0));
	}
}
