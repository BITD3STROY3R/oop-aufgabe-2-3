/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import java.util.ArrayList;
import java.util.Collection;

import pumpkin.*;
import soil.Dirt;
import soil.Soil;
import weather.WeatherCondition;

public class Test {
	
	public static void run() {
		test1();
		test2();
		test3();
	}

	public static void test1() {
		WeatherCondition weather;
		Simulation simulation;
		Pumpkin pumpkin;
		Soil soil;
		int startWeight;
		
		System.out.println();
		System.out.println("-----------------------------------------"
				+ "[Simulation Test 1] : Teste reaktionen auf Wetter"
				+ "-----------------------------------------");
		
		// Test 1.1
		weather = new WeatherCondition(10.0, 25.0, 4.5, 4.5, 0);
		pumpkin = new Hokaido();
		soil = new Dirt(45);
		startWeight = pumpkin.getWeight();
		System.out.println();
		System.out.println("[Test 1.1] : K\u00fcrbis w\u00e4chst unter g\u00fcnstigen Bedingungen");
		System.out.println("[t0] Kuerbis [" + pumpkin + "]");
		System.out.println("[t1] Wetter [" + weather + "]");
		simulation = new Simulation(pumpkin, soil);
		simulation.nextDay(weather);
		System.out.println("[t1] Kuerbis [" + pumpkin + "]");
		if (startWeight < pumpkin.getWeight()) {
			System.out.println("[[Test erfolgreich]]");
		}
		
		
		// Test 1.2
		weather = new WeatherCondition(3.0, 5.0, 1.0, 1.5, 0);
		pumpkin = new Hokaido();
		soil = new Dirt(45);
		startWeight = pumpkin.getWeight();
		System.out.println();
		System.out.println("[Test 1.2] : Kuerbis waechst nicht unter unguenstigen Bedingungen");
		System.out.println("[t0] Kuerbis [" + pumpkin + "]");
		System.out.println("[t1] Wetter [" + weather + "]");
		simulation = new Simulation(pumpkin, soil);
		simulation.nextDay(weather);
		System.out.println("[t1] Kuerbis [" + pumpkin + "]");
		if (startWeight == pumpkin.getWeight()) {
			System.out.println("[[Test erfolgreich]]");
		}
		
	}
	
	public static void test2() {
		
		Collection<WeatherCondition> weather = new ArrayList<WeatherCondition>();
		Pumpkin pumpkin = new TestPumpkin();
		Soil soil = new Dirt(45);
		
		for (int i = 0; i < pumpkin.getTimeToRipe() + 1; i++) {
			weather.add(new WeatherCondition(10.0, 25.0, 4.5, 4.5, 0));
		}
		
		System.out.println();
		System.out.println("-----------------------------------------"
							+ "[Simulation Test 2]: Teste Trigger"
							+ "-----------------------------------------");
		
		System.out.println();
		System.out.println("[Test 2.1] : Kuerbis wird nach seiner Wachstumsphase reif, wenn die Bedingungen guenstig sind");
		Simulation simulation = new Simulation(pumpkin, soil);
		System.out.println("[Tag "+0+"]: " + pumpkin);
		int j = 1;
		for (WeatherCondition wc : weather) {
			simulation.nextDay(wc);
			System.out.println("[Tag "+j+"]: " + pumpkin);
			j++;
		}
		if (pumpkin.isRipe() == true) {
			System.out.println("[[Test erfolgreich]]");
		}
		
		System.out.println();
		System.out.println("[Test 2.2] : Reife wird durch zu viel Duenger aufgehalten");
		pumpkin = new TestPumpkin();
		soil = new Dirt(100);
		simulation = new Simulation(pumpkin, soil);
		int days = pumpkin.getTimeToRipe();
		int maxWeight = 0;
		for (int i = 0; i < days*3; i++) {
			simulation.nextDay((new WeatherCondition(10.0, 25.0, 4.5, 4.5, 0)));
			soil.increaseFertility(100);
			maxWeight = (i == days + 1) ? pumpkin.getWeight() : maxWeight;
			System.out.println("[Tag " + (i+1) + "]: " + pumpkin);
		}
		if (pumpkin.getWeight() < maxWeight && !pumpkin.isRipe()) {
			System.out.println("[[Test Erfolgreich]]");
		}
	}
	
	public static void test3() {
		
		Collection<WeatherCondition> weather = new ArrayList<WeatherCondition>();
		Pumpkin pumpkin = new TestPumpkin();
		Soil soil = new Dirt(0);
		int initialWeight1 = pumpkin.getWeight();
		int finalWeight1 = 0;
		
		for (int i = 0; i < pumpkin.getTimeToRipe() + 1; i++) {
			weather.add(new WeatherCondition(10.0, 25.0, 10.0, 4.5, 0));
		}
		
		System.out.println();
		System.out.println("-----------------------------------------"
				+ "[Simulation Test 3]: Teste Ungeziefer und Pestizide bzw. CareOperations"
				+ "-----------------------------------------");
		
		System.out.println();
		System.out.println("[Test 3.1]: Schaden durch Schnecken (Faeulnis nicht aktiviert um Ueberschneidungen auszuschlieszen)");
		Simulation simulation = new Simulation(pumpkin, soil);
		simulation.disablePutrefaction();
		System.out.println("[Tag 0] : " + pumpkin);
		int i = 1;
		for (WeatherCondition wc : weather) {
			simulation.nextDay(wc);
			System.out.println("[Tag "+i+"] : " + pumpkin);
			i++;
		}
		System.out.println(pumpkin);
		finalWeight1 = pumpkin.getWeight();
		if (initialWeight1 > finalWeight1) {
			System.out.println("[[Test erfolgreich]]");
		}
		
		System.out.println();
		System.out.println("[Test 3.2]: Gleiche Bedingungen wie 3.1, und Pestizid bei Tag 0, Dauer 2, Boomerangeffekt erhoeht schaeden gegen ende der Wirksamkeit");

		pumpkin = new TestPumpkin();
		soil = new Dirt(0);
		simulation = new Simulation(pumpkin, soil);
		simulation.disablePutrefaction();
		simulation.addCare(new AntiSlug(2));
		System.out.println("[Tag 0] : " + pumpkin);
		i = 1;
		for (WeatherCondition wc : weather) {
			simulation.nextDay(wc);
			System.out.println("[Tag "+i+"] : " + pumpkin);
			i++;
		}
		System.out.println(pumpkin);
		if (pumpkin.getWeight() < finalWeight1) {
			System.out.println("[[Test erfolgreich]]");
		}
		
		System.out.println();
		System.out.println("[Test 3.2]: Gleiche bedingungen wie 3.1 und 3.2, und Pestizid bei Tag 0, Dauer 10, Boomerangeffekt wird nicht erreicht");
		
		pumpkin = new TestPumpkin();
		soil = new Dirt(0);
		simulation = new Simulation(pumpkin, soil);
		simulation.disablePutrefaction();
		simulation.addCare(new AntiSlug(10));
		System.out.println("[Tag 0] : " + pumpkin);
		i = 1;
		for (WeatherCondition wc : weather) {
			simulation.nextDay(wc);
			System.out.println("[Tag "+i+"] : " + pumpkin);
			i++;
		}
		System.out.println(pumpkin);
		if (pumpkin.getWeight() > finalWeight1) {
			System.out.println("[[Test erfolgreich]]");
		}
	}
}
