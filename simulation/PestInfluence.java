/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import java.util.Iterator;

import pumpkin.Pumpkin;

public class PestInfluence implements PumpkinAlteration {

	// invariante: e != null
	private Environment e;
	
	// vorbedingung: e != null
	public PestInfluence(Environment e) {
		this.e = e;
	}
	
	@Override
	public boolean hasWornOff() {
		return false;
	}

	// vorbedingung: pumpkin != null
	// nachbedingung: verringer das Gewicht von pumpkin und
	// entfernt alle elemente aus e.pest.
	// FEHLER die nachbedingung ist schwächer als die des Obertyps. e dürfte nicht verändert
	// werden da sich der client darauf verlässt das höchstens pumpkin verändert wird.
	@Override
	public void apply(Pumpkin pumpkin) {
		
		int sumDamage = 0;
		Iterator<Pest> it = e.getPest().iterator();
		
		// sum damage and clear the current pests
		while (it.hasNext()) {
			sumDamage += it.next().getDamage();
			it.remove();
		}
		
		pumpkin.decreseWeightAbsolute(sumDamage);
	}
}
