/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import pumpkin.Pumpkin;

public class Putrefaction implements PumpkinAlteration {

	private boolean done = false;
	
	// invariante env != null
	private Environment env;
	
	// vorbedingung env != null
	public Putrefaction(Environment env) {
		this.env = env;
	}
	
	@Override
	public boolean hasWornOff() {
		return done == true;
	}

	// vorbedingung: p != null
	// nachbedingung: fügt p die Schäden durch Fäulnis hinzu.
	@Override
	public void apply(Pumpkin p) {
		
		Range<Integer> reqHumidity = p.getHumidity();
		
		int putrefaction = (int)Math.abs(env.getHumidity()
				- reqHumidity.getHigh()
				/ (reqHumidity.width() / 100));
		
		// vorbedingung für p.decreaseWeightPercent absichern
		putrefaction = putrefaction > 100 ? 100 : putrefaction;
		
		// Apply changes to pumpkin
		p.decreaseWeightPercent(putrefaction * p.getResistanceToPutrefaction() / 100);
		
		done = true;
	}
}
