/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import soil.Soil;

public abstract class SoilOperation implements CareOperation {
	
	// invariante: ttl >= 0
	private int ttl;
	
	// vorbedingung: ttl >= 0
	public SoilOperation(int ttl) {
		this.ttl = ttl;
	}
	
	@Override
	public boolean hasWornOff() {
		return ttl == 0;
	}
	
	/* vorbedingung: e != null
	 * nachbedingung: verändert e entsprechend der Eigenschaften
	 * von soil falls ttl > 0, dekrementiert ttl.
	 * SCHLECHT die manuelle überprüfung von ttl > 0 hätte
	 * durch die vorbedingung !hasWornOff() vermieden werden können
	 */
	@Override
	public void apply(Environment e) {
		
		if (ttl > 0) {
			Soil soil = e.getSoil();
		
			apply(soil);
			
			ttl--;
		}
	}
	
	public abstract void apply(Soil soil);
}
