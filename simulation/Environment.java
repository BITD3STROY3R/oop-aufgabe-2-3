/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import java.util.Collection;
import java.util.LinkedList;

import soil.Soil;

public class Environment {
	
	// invariante: pest != null
	private Collection<Pest> pest = new LinkedList<Pest>();
	// invariante soil != null
	private Soil soil;
	
	// invariante: 0 <= light <= 24
	private double light;
	// invariante: humidity >= 0
	private int humidity;
	// invariante: wind >= 0
	private int wind;
	// invariante: temperture != null
	private Range<Integer> temperature;
	
	// vorbedingung: soil != null
	public Environment(Soil soil) {
		this.soil = soil;
		this.light = 0;
		this.humidity = 0;
		this.wind = 0;
		this.temperature = new Range<Integer>(0,0);
	}
	
	// nachbedingung: liefert referenz ungleich null
	public Collection<Pest> getPest() {
		return pest;
	}
	
	// nachbedingung: liefert referenz ungleich null
	public Soil getSoil() {
		return soil;
	}
	
	// nachbedingung: liefert x, 0 <= x <= 24
	public double getLight() {
		return light;
	}
	
	// nachbedingung: liefert x >= 0
	public int getWind() {
		return wind;
	}
	
	// nachbedingung: liefert referenz ungleich null
	public Range<Integer> getTemperature() {
		return temperature;
	}
	
	// nachbedingung: liefert x >= 0
	public int getHumidity() {
		return humidity;
	}
	
	// vorbedingung: wind >= 0
	public void setWind(int wind) {
		this.wind = wind;
	}

	// vorbedingung: humidity >= percent
	// nachbedingung: zieht percent von humidity ab
	public void decreaseHumidity(int percent) {
		humidity -= percent;
		// SCHLECHT diese Zeile ist überflüssig da die vorbedingung humidity < 0 ausschließt
		humidity = humidity < 0 ? 0 : humidity;
	}
	
	// vorbedingung: percent >= 0
	public void increaseHumidity(int percent) {
		humidity += percent;
	}
	
	// vorbedingung: 0 <= light <= 24
	public void setLight(double light) {
		this.light = light;
	}
	
	// vorbedingung: temperature != null
	public void setTemperature(Range<Integer> temperature) {
		this.temperature = temperature;
	}
}
