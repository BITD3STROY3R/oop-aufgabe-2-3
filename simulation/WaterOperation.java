/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

public class WaterOperation implements CareOperation {

	private int cost = 10;
	
	/* invariante quantity = quantity und quantity >= 0
	 * SCHLECHT quantity hätte entsprechend der invarianten auch als final
	 * deklariert werden können
	 */
	private int quantity;
	private boolean done = false;
	
	// vorbedingung: quantity >= 0
	public WaterOperation(int quantity) {
		this.quantity = quantity;
	}
	
	// vorbedingung e != null
	// nachbedingung: erhöht die feuchtikeit von e um quantity
	// setzt done = true
	@Override
	public void apply(Environment e) {
		
		done = true;
		e.increaseHumidity(quantity);
	}

	@Override
	public boolean hasWornOff() {
		return done;
	}

	@Override
	public int getCost() {
		return cost;
	}

}
