/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;

import pumpkin.Hokaido;
import pumpkin.Pumpkin;
import soil.*;
import weather.AustrianWeatherGenerator;
import weather.WeatherCondition;
import weather.WeatherGenerator;

public class Simulation {
	
	private Pumpkin pumpkin;
	private Environment environment;
	
	private LinkedList<PumpkinAlteration> pumpkinAlterations
		= new LinkedList<PumpkinAlteration>();
	
	private LinkedList<EnvironmentAlteration> envAlterations
		= new LinkedList<EnvironmentAlteration>();
	
	private Collection<PumpkinTrigger> pumpkinTriggers
		= new ArrayList<PumpkinTrigger>();
	
	private Collection<EnvironmentTrigger> environmentTriggers
		= new ArrayList<EnvironmentTrigger>();
	
	// Initializes a new Simulation
	public Simulation(Pumpkin pumpkin, Soil soil) {
		this.pumpkin = pumpkin;
		this.environment = new Environment(soil);
		
		pumpkinAlterations.clear();
		envAlterations.clear();
		
		// add Triggers
		pumpkinTriggers.add(new PutrefactionTrigger(pumpkin, environment));
		pumpkinTriggers.add(new RipeTrigger(pumpkin, environment));
	
		environmentTriggers.add(new SlugTrigger(environment));
		environmentTriggers.add(new LocustTrigger(environment));
		
		// permanent environmental alterations
		envAlterations.add(new SoilInfluence());
		
		// permanent pumpkin alterations
		pumpkinAlterations.add(new PestInfluence(environment));
		
		// pumpkin Growth influences both, the Pumpkin and the Environment
		PumpkinGrowth pg = new PumpkinGrowth(pumpkin, environment);
		pumpkinAlterations.addLast(pg);
		envAlterations.addLast(pg);
	}
	
	/* vorbedingung: co != null
	 * nachbedingung: fÃ¼gt die pflege co hinzu.
	 */
	public void addCare(CareOperation co) {
		envAlterations.addLast(co);
	}
	
	/* vorbedingung: wc != null
	 * nachbedingung: simuliert den nÃ¤chsten Tag wobei wc das Wetter ist
	 * GUT: Dynamisches Binden erleichtert das ausführen der effekte auf kürbis/boden
	 */
	public void nextDay(WeatherCondition wc) {
		// handle Weather
		WeatherInfluence wi = new WeatherInfluence(wc);
		wi.apply(environment);
		
		// handle pumpkin
		Iterator<PumpkinAlteration> it2 = pumpkinAlterations.iterator();
		while(it2.hasNext()) {
			PumpkinAlteration pa = it2.next();
			
			if (pa.hasWornOff()) {
				it2.remove();
			} else {
				pa.apply(pumpkin);
			}
		}
		
		// execute triggers
		handleTriggers();

		// handle environment
		Iterator<EnvironmentAlteration> it1 = envAlterations.iterator();
		while (it1.hasNext()) {
			EnvironmentAlteration ea = it1.next();
			
			if (ea.hasWornOff()) {
				it1.remove();
			} else {
				ea.apply(environment);
			}
		}
	}
	
	/* client-controlled history constraint: soll nur einmal pro Tag ausgefÃ¼hrt werden
	 * nachbedingung: Ã¼berprÃ¼ft ob die trigger gefeuert haben
	 * und fÃ¼gt ggf. alterations hinzu.
	 */
	private void handleTriggers() {
		
		// try to fire Pumpkin alterations
		for (PumpkinTrigger at : pumpkinTriggers) {
			if (at.hasFired()) {
				// triggered alterations have higher priority
				pumpkinAlterations.addFirst(at.generateAlteration());
			}
		}
		
		// try to fire Environment alterations
		for (EnvironmentTrigger et : environmentTriggers) {
			if (et.hasFired()) {
				// triggered alterations have higher priority
				envAlterations.addFirst(et.generateAlteration());
			}
		}
	}
	
	// nachbedingung: schaltet alle Putrefaction trigger ab
	// for debug purpose only
	public void disablePutrefaction() {
		Iterator<PumpkinTrigger> it = pumpkinTriggers.iterator();
		while (it.hasNext()) {
			if (it.next() instanceof PutrefactionTrigger) {
				it.remove();
			}
		}
	}
	

	public static void main(String[] args) {

		WeatherGenerator gen = new AustrianWeatherGenerator(
				new GregorianCalendar(2000, 0, 1), 
				new GregorianCalendar(2001, 0, 1)
		);

		Pumpkin pumpkin = new Hokaido();
		Soil soil		= new Dirt(100);

		Simulation sim = new Simulation(pumpkin, soil);
		
		for (WeatherCondition wc : gen.getWeather()) {
			sim.nextDay(wc);
			System.out.println(pumpkin);
		}
		
		System.out.println(pumpkin);
		System.out.println("Terminated without errors");
	}
}
