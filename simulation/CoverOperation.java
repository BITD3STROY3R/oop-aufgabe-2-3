/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

public class CoverOperation implements CareOperation {

	// invariante ttl >= 0
	private int ttl;
	// invariante cost >= 0
	private final int cost;
	private final int costPerDay = 50;
	
	// vorbedingung: ttl > 0
	public CoverOperation(int ttl) {
		this.ttl = ttl;
		this.cost = ttl * costPerDay;
	}
	
	// vorbedingung: e != null
	// nachbedingung: e.light = 0, e.wind = 0
	@Override
	public void apply(Environment e) {
		e.setLight(0);
		e.setWind(0);
	}

	@Override
	public boolean hasWornOff() {
		return ttl == 0;
	}

	@Override
	public int getCost() {
		return cost;
	}

}
