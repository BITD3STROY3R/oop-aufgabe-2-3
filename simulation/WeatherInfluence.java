/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import weather.WeatherCondition;

public class WeatherInfluence implements EnvironmentAlteration {

	// condition != null
	private WeatherCondition condition;
	
	// condition != null
	public WeatherInfluence(WeatherCondition condition) {
		this.condition = condition;
	}
	
	@Override
	public boolean hasWornOff() {
		throw new UnsupportedOperationException();
	}

	/* e != null
	 * verändert e enstprechend der wetterlage condition
	 */
	@Override
	public void apply(Environment e) {
		
		// draining calculated by alteration
		e.increaseHumidity((int)condition.getRain()*10);
		
		// wind handled by alteration 
		e.setWind((int)condition.getWind());
		
		// light handled by alteration
		e.setLight(condition.getSunHours());
		
		// temperature handled by alteration
		e.setTemperature(new Range<Integer>((int)condition.getLow(),
				(int)condition.getHigh()));
	}

}
