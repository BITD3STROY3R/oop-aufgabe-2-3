/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import pumpkin.Pumpkin;

public class PutrefactionTrigger implements PumpkinTrigger {
	
	// GUT: Klassenzusammenhalt bei allen Triggern, hätte man mit vielen ifs wesentlich schlechter lösen können

	// env != null
	private Environment env;
	// p != null
	private Pumpkin p;
	
	// p != null und env != null
	public PutrefactionTrigger(Pumpkin p, Environment env) {
		this.env = env;
		this.p = p;
	}

	@Override
	public Putrefaction generateAlteration() {
		return new Putrefaction(env);
	}

	@Override
	public boolean hasFired() {
		return env.getHumidity() > p.getHumidity().getHigh()
				|| (p.getTimeToRipe() == 0 
					&& env.getSoil().getFertility() > p.getFertility().getHigh()); 
	}

}
