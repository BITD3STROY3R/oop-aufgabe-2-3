/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

public class AntiLocust extends AntiPestOperation {

	// cost >= 0
	private final int cost;
	
	private final int costPerDay = 5;
	private final int boomerangPeriod = 3;
	
	// ttl > 0
	public AntiLocust(int ttl) {
		super(ttl);
		this.cost = ttl * costPerDay;
	}
	
	// Liefert die kosten
	@Override
	public int getCost() {
		return cost;
	}

	/*
	 * Entfernt alle Element aus pest oder
	 * fügt zu den bestehenden elementen neue hinzu.
	 */
	@Override
	public void apply(Collection<Pest> pest) {
		
		// normal effect
		if (getTtl() > boomerangPeriod) {
			Iterator<Pest> it = pest.iterator();
			
			// kill all pests of type Locust
			while (it.hasNext()) {
				if (it.next() instanceof Locust) {
					it.remove();
				}
			}
		// boomerang effect
		} else {
			
			// generate some new Locusts, 1(1)boomerang
			int number = (new Random().nextInt() % boomerangPeriod) + 1;
			for (int i = 0; i < number; i++) {
				pest.add(new Locust());
			}
		}
	}

}
