/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import java.util.Collection;

public abstract class AntiPestOperation implements CareOperation {

	// invariante: ttl >= 0
	private int ttl;
	
	// vorbedingung: ttl > 0
	public AntiPestOperation(int ttl) {
		this.ttl = ttl;
	}
	
	// vorbedingung:	e != null
	// nachbedingung:	e.pest verändert, e.pest != null
	@Override
	public void apply(Environment e) {
		
		if (ttl > 0) {
			apply(e.getPest());
			ttl--;
		}
	}
	
	@Override
	public boolean hasWornOff() {
		return ttl == 0;
	}
	
	// Liefert den Wert von ttl
	protected int getTtl() {
		return ttl;
	}
	
	// nachbedingung:	verändert p
	public abstract void apply(Collection<Pest> p);
}
