/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import java.util.Collection;
import java.util.Iterator;

public class AntiSlug extends AntiPestOperation {

	// invariante: cost >= 0
	private final int cost;
	
	private final int costPerDay = 10;
	private final int boomerangPeriod = 2;
	
	// vorbedingung: ttl > 0
	public AntiSlug(int ttl) {
		super(ttl);
		cost = ttl * costPerDay;
	}
	
	// vorbedingung:	pest != null
	// nachbedingung:	pest verändert
	@Override
	public void apply(Collection<Pest> pest) {
		
		// normal effect
		if (getTtl() > boomerangPeriod) {
			Iterator<Pest> it = pest.iterator();
			
			// kill all pests of type slug
			while (it.hasNext()) {
				if (it.next() instanceof Slug) {
					it.remove();
				}
			}
		// boomerang effect
		} else {
			
			// generate some new Slugs, 1(1)boomerang
			int number = (boomerangPeriod) + 1;
			for (int i = 0; i < number; i++) {
				pest.add(new Slug());
				pest.add(new Slug());
			}
		}
	}
	
	@Override
	public int getCost() {
		return cost;
	}

}
