/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import soil.Soil;

public class FertilizeOperation extends SoilOperation {

	private final int cost = 50;
	// invariante: quantity >= 0
	private final int quantity;
	
	// vorbedingung quantity >= 0
	public FertilizeOperation(int quantity) {
		super(1);
		this.quantity = quantity;
	}
	
	@Override
	public int getCost() {
		return cost;
	}

	// vorbedingung: soil != null
	// nachbedingung: erhöht soil.fertility um quantity
	@Override
	public void apply(Soil soil) {
		soil.increaseFertility(quantity);
	}

}
