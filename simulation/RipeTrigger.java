/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simulation;

import pumpkin.Pumpkin;

public class RipeTrigger implements PumpkinTrigger {

	// invariante pumkin != null und environment != null
	private Pumpkin pumpkin;
	private Environment environment;
	
	// vorbedingung: pumkin != null und environment != null
	public RipeTrigger(Pumpkin p, Environment e){
		this.pumpkin = p;
		this.environment = e;
	}
	
	@Override
	public boolean hasFired() {
		return !pumpkin.isRipe() 
				&& pumpkin.getTimeToRipe() == 0 
				&& pumpkin.getFertility().getHigh() > (environment.getSoil().getFertility())
				;
	}

	@Override
	public PumpkinAlteration generateAlteration() {
		return new RipeAlteration();
	}

}
