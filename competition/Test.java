/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package competition;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import competition.Competition;

import pumpkin.Butternut;
import pumpkin.Hokaido;
import pumpkin.Jackolantern;
import simulation.FertilizeOperation;
import soil.Clay;
import soil.Dirt;
import soil.Sand;

public class Test {

	public static void run() {
		
		System.out.println();
		System.out.println("-----------------------------------------"
				+ "[Comptition Test 1] : Beispieldurchlauf eines Wettbewerbs"
				+ "-----------------------------------------");
		Calendar start = new GregorianCalendar(2013, 2, 15);
		Calendar end = new GregorianCalendar(2013, 10, 30);
		
		Competition c = new Competition(start, end);
		
		User u1 = new User("Stefan Hanreich", new Jackolantern(), new Dirt(100));
		User u2 = new User("Bernd Bogomolov", new Hokaido(), new Sand(100));
		User u3 = new User("Jannik Vierling", new Jackolantern(), new Dirt(100));
		//User u4 = new User("Jannik Vierling", new Hokaido(), new Clay(100));
		//System.out.println(u1.getP() + " " + u1.getS());
		
		c.addCompetitor(u1);
		c.addCompetitor(u2);
		c.addCompetitor(u3);
		u3.addOperation(new GregorianCalendar(2013, 2, 15), new FertilizeOperation(1), c.getMaxRess());

		
		u1.addOperation(new GregorianCalendar(2013, 2, 15), new FertilizeOperation(1), c.getMaxRess());
		
		c.nextDay();
		c.nextDay();
		c.addCompetitor(u3);
		
		
		c.allDays();		
		
		
		
		Map<User, Integer> results = c.getResults();
		User winner = null;
		for (Map.Entry<User, Integer> result : results.entrySet()) {
			System.out.println(result.getKey() + ": " + result.getValue());
			if (winner == null) {
				
			}
		}
	}

}
