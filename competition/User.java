/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package competition;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import pumpkin.Pumpkin;
import simulation.CareOperation;
import soil.Soil;

public class User {
	
	private String name;
	private Pumpkin p;
	private Soil s;
	private HashMap<Calendar, List<CareOperation>> operations;
	
	
	//
	// Vorbedingung: name != null && c != null  && s != null
	// 
	public User(String name, Pumpkin p, Soil s) {
		this.name = name;
		this.p = p;
		this.s = s;
		this.operations = new HashMap<Calendar, List<CareOperation>>();
	}
	
	//Vorbeding:  date != null && op != null
	 
	public void addOperation(Calendar date, CareOperation op, int maxRess){
		int ress = getOperationCost(date);
		if (ress + op.getCost() <= maxRess) {
			if (operations.get(date) != null) {
				operations.get(date).add(op);
			}
			else{
				List<CareOperation> ops = new ArrayList<CareOperation>();
				ops.add(op);
				operations.put(date, ops);
			}
		}
		else{
			System.out.println("Didnt add operation: Too much resources");
		}
	}
	
	// Vorbedingung: date != nulL
	//Nachbedingung  return der korrekten ress;
	
	private int getOperationCost(Calendar date){
		List<CareOperation> ops = operations.get(date);
		int ress = 0;
		if (ops != null) {
			for (CareOperation op : ops) {
				ress += op.getCost();
			}
		}

		return ress;
	}
	
	public String getName() {
		return name;
	}

	public Pumpkin getP() {
		return p;
	}

	public Soil getS() {
		return s;
	}

	public List<CareOperation> getOperations(Calendar date) {
		return operations.get(date);
	}
	
	public String toString(){
		return name;
	}

}
