/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package competition;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pumpkin.Pumpkin;
import simulation.CareOperation;
import simulation.Simulation;
import weather.AustrianWeatherGenerator;
import weather.WeatherCondition;

public class Competition {
	
	private Iterator<WeatherCondition> weatherIterator;
	private HashMap<User, Simulation> competitors;
	private HashMap<User, Pumpkin> pumpkins;
	private int maxRess = 100;
	private Calendar today;
	
	// Konstruktor Vorbedinung start != null && end != null
	// Konstruktor Nachbedingung: erstellen eines lauffaehigen Competition objekts
	// SCHLECHT: assert() verwenden da Competition mit NULL Werten erzeugt werden koennte bei DAU
	public Competition(Calendar start, Calendar end){
		competitors = new HashMap<User, Simulation>();
		pumpkins = new HashMap<User, Pumpkin>();
		weatherIterator = new AustrianWeatherGenerator(start, end).getWeather().iterator();
		this.today = (Calendar) start.clone();
	}
	
	// Vorbedingung: User u != null  && Competition ohne NULL-Werte erzeugt
	// Legt den User als Competitor an 
	// Nachbedingung: User wurde in der Competiotor und pumkin Hashmap angelegt
	public void addCompetitor(User u){
		competitors.put(u, new Simulation(u.getP(), u.getS()));
		pumpkins.put(u, u.getP());
	}
	//
	// Vorbedingung: Competition ohne NULL-Werte erzeugt
	// Nachbedinung: vom aktuellen Tag wird auf den naechsten Tag "geschaltet" 
	public void nextDay(){
		if (hasNextDay()) {
			WeatherCondition today = weatherIterator.next();
			
			for (Map.Entry<User, Simulation> entry : competitors.entrySet()) {
			    User u = entry.getKey();
			    Simulation s = entry.getValue();
			    List<CareOperation> ops = u.getOperations(this.today);
			    if (ops != null) {
			    	for (CareOperation op : ops) {
						s.addCare(op);
					}
				}
			    
			    s.nextDay(today);
			}
			this.today.add(Calendar.DATE, 1);
		}
		else{
			System.out.println("No next day");
		}
	}
	
	
	// Nachbedingung: Durchlauf aller Tage
	public void allDays(){
		while (hasNextDay()) {
			nextDay();
		}
	}
	
	// Vorbedingung: Um die korrekten Ergebnise des Wettbewerbs zu erhalten muss der Wettbewerb geendet haben
	// getResults()
	// Nachbedingung:  Hashmap <User, Integer> wird mit den Ergebnisen returned
	
	public Map<User, Integer> getResults(){
		HashMap<User, Integer> results = new HashMap<User, Integer>();
		for (User u : competitors.keySet()) {
			if (pumpkins.get(u).isRipe()) {
				results.put(u, pumpkins.get(u).getWeight());
			}
			else{
				results.put(u, -1);
			}
		}
		
		return results;
	}
	
	public int getMaxRess(){
		return maxRess;
	}
	
	public boolean hasNextDay(){
		return weatherIterator.hasNext();
	}

}