/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 2+3 
 * ===========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package soil;

import java.util.Collection;
import java.util.ArrayList;

public abstract class Soil {

	// fertitity >= 0
	private int fertility;
	// drainage >= 0
	private int drainage;
	// 0 <= compression <= 1
	private double compression;
	// 0 <= weed
	private double weed;
	// 0 <= compressionPotential 
	private double compressionPotential;
	// 0 <= weedPotential <= 1
	private double weedPotential;
	
	/* fertitity >= 0 und drainage >= 0 und 0 <= compression <= 1 und 
	 * 0 <= weed und 0 <= compressionPotential und 0 <= weedPotential <= 1
	 */
	public Soil(int fertility, int drainage, double compression, 
			double compressionPotential, double weed, double weedPotential) {
		this.fertility = fertility;
		this.drainage = drainage;
		this.compression = compression;
		this.compressionPotential = compressionPotential;
		this.weed = weed;
		this.weedPotential = weedPotential;
	}
	
	public int getFertility() {
		return fertility;
	}
	
	public int getDrainage() {
		return (int) (drainage - drainage * compression);
	}
	
	/* simuliert eine "Abnutzung" des Bodens.
	 * Basierend auf den aktuellen Werten werden fertility, compression
	 * und weed sich verändern.
	 * Client Controlled History Constraint: nur einmal pro "Tag" aufrufen
	 */
	public void refresh() {
		computeCompression();
		computeWeed();
		computeFertility();
	}
	
	/* 0 <= percent <= 100
	 * Verringert compression um percent %
	 */
	public void decreaseCompression(int percent) {
		compression -= compression * (double) percent / 100;
		// SCHLECHT überprüfung hätte durch hinzufügen einer 
		// Vorbedingung vermieden werden können
		compression = compression < 0.0 ? 0.0 : compression;
	}
	
	/* 0 <= percent <= fertility
	 * Zieht percent von fertility ab
	 */
	public void decreaseFertility(int percent) {
		fertility -= percent;
		// SCHLECHT überprüfung hätte durch hinzufügen einer 
		// Vorbedingung vermieden werden können
		fertility = fertility < 0 ? 0 : fertility;
	}
	
	/* 0 <= percent
	 * Addiert percent zu fertility
	 */
	public void increaseFertility(int percent) {
		fertility += percent;
	}
	
	/* 0 <= percent <= 100
	 * Verringert weed um percent %
	 */
	public void decreaseWeed(int percent) {
		weed -= percent / (double) 100;
		weed = weed < 0 ? 0 : weed;
	}
	
	/* berechnet die zeitlich bedingte verdichtung
	 * und weist diese compression zu
	 * Client Controlled History Constraint: nur einmal pro "Tag" aufrufen
	 */
	private void computeCompression() {
		compression += compressionPotential;
		compression = compression > 1.0 ? 1.0 : compression;
	}
	
	/* berechnet den zeitlich bedingten Unkrautwuchs
	 * und weist weed den neun Wert zu.
	 * Client Controlled History Constraint: nur einmal pro "Tag" aufrufen
	 */
	private void computeWeed() {
		
		if (fertility > 75) {
			weed += (double) fertility * weedPotential / 100;
		} else if (fertility > 15) {
			weed += weedPotential / 100;
		} else {
			weed -= weedPotential;
		}
		
		weed = weed < 0.0 ? 0.0 : weed;
	}
	
	/* berechnet die durch Unkraut bedingte verbleibende Fruchchtbarkeit
	 * und weist fertility diesen Wert zu
	 * Client Controlled History Constraint: nur einmal pro "Tag" aufrufen
	 */
	private void computeFertility() {
		fertility -= Math.ceil(weed);
		fertility = fertility < 0 ? 0 : fertility;
	}
	
	public String toString() {
		return String.format("fertility = %d"
				+ "; drainage = %d"
				+ "; compression = %3.2f"
				+ "; weed = %3.2f", fertility, drainage, compression, weed);
	}
	public static void main(String[] args) {
		
		Collection<Soil> soils = new ArrayList<Soil>();
		soils.add(new Sand(100));
		soils.add(new Clay(100));
		soils.add(new Dirt(100));
		
		for (Soil s : soils) {
			System.out.println();
			System.out.println("[Testing] : " + s);
			System.out.println();
			for (int i = 0; i < 100; i++) {
				s.refresh();
				System.out.println(" [" + i + "] " + s);
			}
		}
	}
}
